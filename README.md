# README #


### What is this repository for? ###

* To allow each citizen to inform the rest of the community on things which are not right in his/her city
* This REST API is built using Django Rest Framework
* The API allows to: register an user, confirm the users activation by following a link in the email, post a new issue
* The issue can include an image and the location is automatically matched via Google Maps API to a place on the map.
* Each issue can have comments
* Only the user who created an issue can delete it or edit it
* The API is exposed over JSON
* Login and auth happen via JWT, which is stored in localStorage client side.
* The backend integrates with Google Maps API service to accurately match a user input location with a valid one on Maps.
* The LIVE staging site is available at: http://youcitizen.radiolondra.co.uk


### How do I get set up? ###

* Clone the repo
* install and run virtualenvwrapper
* Run: `pip install virtualenvwrapper`
* Install requirements: `pip install -r requirements.txt`
* Then: `source /usr/local/bin/virtualenvwrapper.sh`
* Finally: `Workon youcitizen`
* Create `email_user`, `email_password` (with valid data) in root folder to enable actual sending of emails. Currently this is done via Gmail, also in Google developers console enable email third party login.
* Create `google_maps_api_key` file and add a valid Google Maps API key.
* Then run to create database schema: `python manage.py makemigrations youcitizen_app`
* Then create tables: `python manage.py migrate`
* Run Django dev server: `python manage.py runserver`
* Tests are in tests.py and can be run with: `python manage.py tests`
* To send emails with activation link, add an `email_password` and `email_user` files (with a valid gmail address, enabling thirdparty login in the Google API dashboard) in directory with `manage.py`.

### API Endpoint 

Using the DRF the API can be navigated at:

* `http://IP_OF_DRF_DEV_SERVER:PORT/issues/` : to inspect the issues endpoint
* `http://IP_OF_DRF_DEV_SERVER:PORT/users/`  : to inspect users endpoint, requires login.
* http://youcitizen.radiolondra.co.uk:8000/issues/ : staging issues endpoint
* http://youcitizen.radiolondra.co.uk:8000/users/ : staging users endpoint



### TODO FOR PRODUCTION ###

In order to pass from staging to prod, it is needed to:

* Secure the API over HTTPS, so token cannot be sniffed
* ~~Split database in two: one for users one for issues~~ [556d64](https://bitbucket.org/andrea-f/youcitizen-backend/commits/70c66fda11e5f2120bd73669b5556d64)


### TODO FOR FUTURE ###

* Allow signon via social networks
* Allow possibility of uploading more than one picture
* Allow possibility of loading and displaying video files
* Allow possibility to filter by location and tags


### References ###
* Django rest framework
* Django documentation 1.10