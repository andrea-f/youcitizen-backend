FROM python:2.7-slim


RUN set -x \
        && buildDeps=' \
                build-essential \
                git \
                python-dev \
                python-setuptools \
                nginx \
                sqlite3 \
                supervisor \
                libjpeg62-turbo-dev \
                libfreetype6-dev \
                libxft-dev \
                libjpeg62 \
                libjpeg-dev \
                vim \
                netcat-traditional \
        ' \
        && DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y $buildDeps --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN pip install uwsgi

ADD . /opt/django/



RUN pip install -r /opt/django/requirements.txt

RUN apt-get clean
RUN rm -rf /var/tmp
RUN python ./opt/django/manage.py makemigrations youcitizen_app
RUN python ./opt/django/manage.py migrate
RUN python ./opt/django/manage.py collectstatic --no-input
WORKDIR ./opt/django/
CMD ["uwsgi", "--module=youcitizen.wsgi:application", "--static-map=/media=/opt/django/media", "--static-map=/static=/opt/django/youcitizen/static","--env=DJANGO_SETTINGS_MODULE=youcitizen.settings", "--master", "--pidfile=/tmp/youcitizen.pid", "--http=0.0.0.0:8000", "--socket=0.0.0.0:8001", "--buffer-size=32768"]

                                                                                                                                                                                                                                                                                                  