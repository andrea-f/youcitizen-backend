
from django.urls import reverse
from rest_framework import status
from youcitizen_app.views import IssueList, IssueDetail, UserActions
from youcitizen_app.models import Issue, TemporaryUser
from rest_framework.test import APITestCase, APIRequestFactory, APIClient
from location import Locate
import json
from youcitizen_app.serializers import UserSerializer
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.conf import settings
import mock
from youcitizen_app.views import UserActions
from rest_framework.test import APIClient
from django.core import management
from django.test import tag
from pprint import pprint

def mock_data(what):
    """
    Mocks data for testing purposes. 
    """
    mocks = {
        "mock_user": User.objects.create_user(
            username="test123",
            password="test456",
            email="test@test.com"
        ),
        "factory": APIRequestFactory(),
        "client": APIClient(),
        "mock_user_dict": {
            "username": "test_user",
            "email"   : "test123@test.com",
            "password": "test456"
        },
        "issue":{
            "title"      : "lazio", 
            "description": "abcd",
            "location"   : "rome",
            "id"         : "1"
        }
    }
    try:
        return mocks[what]
    except KeyError:
        if 'all' in what:
            return mocks
        return {}
        


class IssuesTests(APITestCase):

    def setUp(self):
        mocks = mock_data('all')
        self.mock_user      = mocks["mock_user"]
        self.factory        = mocks["factory"]
        self.client         = mocks["client"]
        self.mock_user_dict = mocks["mock_user_dict"]
        self.issue          = mocks["issue"]

    def tearDown(self):
        self.mock_user      = None
        self.factory        = None
        self.client         = None
        self.mock_user_dict = None
        self.issue          = None

    #@tag('skip')
    def test_POST_create_issue_with_credentials(self):
        """
        Ensure we can create a new account object.
        """
        self.client.force_authenticate(user=self.mock_user)
        issue = {
            "title": "Test Issue",
            "description": "Test Issue Description",
            "location": "Test Location"
        }
        response = self.client.post('/issues/', issue, format='json' )
        assert response.data['title'] == issue['title']

    @tag('skip')
    def test_POST_create_fail_without_credentials(self):
        """
        Test that the API returns a 401 Unauthorized if credentials are not provided with the request.
        """
        request = self.factory.post('/issues/', format="json")
        response = IssueList.as_view()(request).render()
        # Check that API returns Unauthorized if credentials were not provided.
        assert response.data['detail'] == "Authentication credentials were not provided."

    @tag('skip')
    def test_GET_all_issues(self):
        """
        Test that the API endpoint to get all issues returns the correct data.
        """
        issue = self.issue
        issue['creator'] = User.objects.all()[0]
        # Create an entry in test database
        Issue.objects.create(**issue)
        # Request the API endpoint
        request = self.factory.get('/issues')
        # Use Mixins to call view
        response = IssueList.as_view()(request).render()
        assert response.status_code == 200


    @tag('skip')
    def test_GET_issue_detail(self):
        """
        Test that API responds with one requested issue.
        """
        issue = self.issue
        issue['creator'] = User.objects.all()[0]
        Issue.objects.create(**issue)
        # Request the API endpoint
        request = self.factory.get('/issue/1')
        response = IssueDetail.as_view()(request, pk=1).render()
        assert response.status_code == 200
        # Convert from ordereddict to dict
        issue =  dict(response.data)
        assert issue['location'] == self.issue['location']

    @tag('skip')
    def test_geolocation_api(self):
        """
        Test that geolocation API works.
        """
        loc = Locate()
        res = loc.find("rome")
        assert res[0]['formatted_address'] == "Rome, Italy"

    @tag('skip')
    def test_GET_all_issues_in_location(self):
        """
        Test that all issues are returned for a specific location.
        """
        issue = self.issue
        issue['creator'] = User.objects.all()[0]
        Issue.objects.create(**issue)
        issue_location = "/issues/{}/".format(self.issue['location'])
        response = self.client.get(issue_location, format="json")
        assert response.status_code == 200
        assert len(dict(response.data)['results']) == 1

    @tag('skip')
    def test_GET_all_issues_by_user(self):
        """
        Test that all issues created by an user are returned.
        """
        self.client.force_authenticate(user=self.mock_user)
        # Get an issue
        issue = self.issue
        # Add an issue to the database.
        response = self.client.post('/issues/', issue, format='json')
        # Add another issue to the database.
        issue['title'] = "Test Issue 2"
        # Create an issue
        response = self.client.post('/issues/', issue, format='json')
        # Get all issues for test user
        user_issues = "/issues/{}/".format(self.mock_user.id)
        response = self.client.get(user_issues, format="json")
        assert response.status_code == 200
        assert dict(response.data)['count'] == 2


    @tag('skip')
    def test_POST_create_comment(self):
        """
        Test that a comment can be added.
        """
        self.client.force_authenticate(user=self.mock_user)
        # Get an issue
        issue = self.issue
        # Add an issue to the database.
        response = self.client.post('/issues/', issue, format='json')
        # Add a comment to the database
        comment = {
            "issue_id": issue['id'],
            "text": "A comment"
        }
        add_comment = '/issue/add_comment/{}/'.format(issue['id'])
        response = self.client.post(add_comment, comment, format="json")
        assert response.status_code == 201
        

    @tag('skip')
    def test_PATCH_issue_by_user(self):
        """
        Test that a user can modify an issue s/he created.
        """
        self.client.force_authenticate(user=self.mock_user)
        # Get an issue
        issue = self.issue
        # Add an issue to the database.
        response_create = self.client.post('/issues/', issue, format='json')
        modify_issue = '/issue/{}/edit/'.format(issue['id'])
        new_issue = issue.copy()
        new_issue['title'] = "Modified Title"
        response = self.client.patch(modify_issue, new_issue, format='json')
        assert response.data['title'] == new_issue['title']
        # Make sure there was a difference before and after the edit
        assert response.data['title'] != response_create.data['title']


class UsersTests(APITestCase):

    def setUp(self):        
        mocks = mock_data('all')
        self.mock_user      = mocks["mock_user"]
        self.factory        = mocks["factory"]
        self.client         = mocks["client"]
        self.mock_user_dict = mocks["mock_user_dict"]
        self.issue          = mocks["issue"]

    def tearDown(self):
        self.mock_user      = None
        self.factory        = None
        self.client         = None
        self.mock_user_dict = None
        self.issue          = None

    @tag('skip')
    def test_signup(self):
        """
        Test that a temporary account is created when the POST request arrives.
        """
        request = self.factory.post('/signup/', self.mock_user_dict, format="json")
        response = UserActions.as_view({"post": "signup"})(request).render()
        resp = dict(response.data)
        assert response.status_code == 201
        assert resp['username'] == self.mock_user_dict['username']

    @tag('skip')
    def test_activation_key(self):
        """
        Test that an account is inactive before user clicks on email link.
        """
        activation_key = "1653f6960f8cf0c4c0c7634c6292c0a8"
        TemporaryUser.objects.create(user=self.mock_user, activation_key=activation_key)
        request = self.factory.post('/signup/', {"activation_key": activation_key}, format="json")
        response = UserActions.as_view({"post": "signup_complete"})(request).render()
        resp = dict(response.data)
        assert resp['email'] == self.mock_user.email
        assert response.status_code == 200

    @tag('skip')   
    def test_email_generation(self):
        """
        Shows the content of the generated email and checks that it contains activation link.
        """
        user_actions = UserActions()
        temp_user = user_actions._create_temp_user(user=self.mock_user)
        site = Site.objects.get_current()
        message = 'Hi {},\n\nPlease activate your account by visiting: http://{}/activate/{}\n\nThank you,\n\nYouCitizen Team'.format(self.mock_user.username, site.domain, temp_user.activation_key)
        subject = "Please activate your account on {}".format(site.domain)
        with mock.patch.object(self.mock_user, 'email_user',
            wraps=self.mock_user.email_user) as activation_email:
            user_actions._signup_send_activation_email(self.mock_user, site)
            activation_email.assert_called_with(subject, message, settings.DEFAULT_FROM_EMAIL)
    
    @tag('skip')
    def test_change_password(self):
        """ Test editing the password for a user. """
        self.client.force_authenticate(user=self.mock_user)
        current_password = self.mock_user_dict['password']
        modify_pwd = '/users/{}/edit_pwd/'.format(self.mock_user.id)
        pwds = {
            "new_password": "newpassword123",
            "current_password": current_password
        }
        response = self.client.patch(modify_pwd, pwds, format='json')
        assert response.status_code == 200

    @tag('inprogress')
    def test_forgotten_password(self):
        """ Test forgotten password """
        request = self.factory.post('/users/reset_pwd', {"email": self.mock_user.email}, format="json")
        # test password is generated and changed
        # test email is sent with new password
