import os
import urllib2
import json
import urllib

GOOGLE_MAPS_API_KEY = "/google_maps_api_key"

class Locate():
    """ Finds a complete address from a location string. """

    def find(self, address):
    	address = urllib.quote_plus(address)
        url = "https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}".format(address, open(os.getcwd()+GOOGLE_MAPS_API_KEY).read())
        location = urllib2.urlopen(url)
        location = json.load(location)
        return location['results']                                     