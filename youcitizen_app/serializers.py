from rest_framework import serializers
from youcitizen_app.models import Issue, Comment
from django.contrib.auth.models import User
from location import Locate
from django.db import models
from drf_extra_fields.fields import Base64ImageField
import django.contrib.auth.password_validation as validators

class EditPasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

class CommentSerializer(serializers.ModelSerializer):
    """
    Handles creation of comments.
    """

    creator  = serializers.ReadOnlyField(source='creator.username')
    issue  = serializers.ReadOnlyField(source='issue.id')

    class Meta:
        model = Comment
        fields = ('id', 'creator', 'text', 'created', 'issue')

    def create(self, validated_data):
        return Comment.objects.create(**validated_data)


class IssueSerializer(serializers.ModelSerializer):
    """
    Serializer to retrieve issues, create and update or delete.
    """

    creator  = serializers.ReadOnlyField(source='creator.username')
    comments = CommentSerializer(many=True, read_only=True)
    image    = Base64ImageField(required=False)

    class Meta:
        model = Issue
        fields = ('id', 'title', 'description', 'location',  'image', 'creator', 'tags', 'created', 'comments')
        ordering = ('created',)



    def create(self, validated_data):
        """
        Create and return a new `Issue` instance, given the validated data.
        """
        try:
            image = validated_data.pop('image')
        except:
            image = None
        
        return Issue.objects.using('issues_db').create(image=image, **validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Issue` instance, given the validated data.
        """
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.location = validated_data.get('location', instance.location)
        instance.image = validated_data.get('image', instance.image)
        instance.tags = validated_data.get('tags', instance.tags)
        instance.creator = validated_data.get('creator', instance.creator)

        instance.save(using='issues_db')
        return instance

    def validate_location(self, data):
        """Validate location"""
        locate = Locate()
        try:
            address = locate.find(data)
            data = address[0]['formatted_address']
        except:
            pass
        return data

class UserSerializer(serializers.ModelSerializer):
    """
    Provides information of user, with added info like comments and issues created.
    """
    issues = serializers.PrimaryKeyRelatedField(many=True, queryset=Issue.objects.all(), default=[])
    comments = serializers.PrimaryKeyRelatedField(many=True, queryset=Comment.objects.all(), default=[])
    email = serializers.EmailField(max_length=254, min_length=None, allow_blank=False)

    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'issues', 'password', 'comments')

    def validate_password(self, data):
        """Validates password against django validators, in settings.py """
        try:
             validators.validate_password(password=data, user=User)
        except exceptions.ValidationError as e:
             raise serializers.ValidationError("Not a valid password, minimum of 8 characters in password, with at least one number.")

    def validate_email(self, data):
        """Validate email"""
        try:
            email_exists = User.objects.filter(email__icontains=data)
            if email_exists:
                raise serializers.ValidationError("Email exists")
        except KeyError:
            raise serializers.ValidationError("Not a valid email")
        return data