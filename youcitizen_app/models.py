from __future__ import unicode_literals
import os
from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
#from django.dispatch import receiver
#from django.db.models.signals import pr/e_save

#@receiver(pre_save, sender=User)
#def use_different_db(self, *args, **kwargs):
#    return super(User, self).using("user_db")

def get_image_path(instance, filename):
	return os.path.join('photos', "%s/%s" % (str(instance.creator), filename))

class Issue(models.Model):
    created     = models.DateTimeField(auto_now_add=True)
    title       = models.CharField(max_length=200, blank=False)
    description = models.TextField(null=True)
    location    = models.CharField(max_length=200, blank=False)
    creator     = models.ForeignKey(User, related_name='issues', on_delete=models.CASCADE)
    image       = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    tags        = models.CharField(max_length=200, blank=True, null=True)



    def get_tags(self):
        return re.split(" ", self.tags)

    def __str__(self):
    	return self.title

    class Meta:
        ordering = ('-created',)



class Comment(models.Model):
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
    text    = models.TextField(null=False)
    created = models.DateTimeField(auto_now_add=True)
    issue   = models.ForeignKey(Issue, related_name='comments', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.text


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

class TemporaryUser(models.Model):
    """
    A simple profile which stores an activation key for use during
    user account registration.
    """
    user           = models.OneToOneField(User, related_name='temporary_user_registration', on_delete=models.CASCADE)
    activation_key = models.CharField(unique=True, blank=False, max_length=40)

