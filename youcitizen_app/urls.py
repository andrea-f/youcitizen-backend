from django.conf.urls import url
from youcitizen_app import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls.static import static
from django.conf import settings


# \w+\s\w+
# \w+|\s
# [a-zA-Z0-9]
# url(r'^issues/(?P<location>[a-zA-Z]+)$', views.IssueList.as_view(), name="issues"),

urlpatterns = [
	url(r'^users/(?P<pk>[0-9]+)/edit_pwd/$', views.EditPassword.as_view()),
    url(r'^issues/(?P<pk>[0-9]+)/$', views.IssueList.as_view(), name="issues"),
	url(r'^issues/(?P<location>.+)/$', views.IssueList.as_view(), name="issues"),
    url(r'^issues/$', views.IssueList.as_view(), name="issues"),
    url(r'^issue/add_comment/(?P<pk>[0-9]+)/$', views.Comment.as_view()),
    url(r'^issue/(?P<pk>[0-9]+)/$', views.IssueDetail.as_view()),
    url(r'^users/signup/$', views.UserActions.as_view({"post": "signup"})),
    url(r'^users/activate/$', views.UserActions.as_view({"post": "signup_complete"})),
    url(r'^user/(?P<username>.+)/$', views.UserDetail.as_view()),
    url(r'^users/$', views.UserList.as_view()),
    url(r'^issue/(?P<pk>[0-9]+)/edit/$', views.IssueDetail.as_view()),
    url(r'^api-token-auth/', views.ObtainAuthTokenWithUser.as_view()),
    
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)

