from __future__ import unicode_literals

from django.apps import AppConfig


class YoucitizenAppConfig(AppConfig):
    name = 'youcitizen_app'

