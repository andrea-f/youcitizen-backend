from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.response import Response
from youcitizen_app.models import Issue, TemporaryUser
from youcitizen_app.serializers import IssueSerializer, UserSerializer, EditPasswordSerializer, CommentSerializer
from django.http import Http404
from rest_framework import mixins
from rest_framework import generics
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.views import APIView
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from youcitizen_app.permissions import IsOwnerOrReadOnly
from django.template.loader import render_to_string
import hashlib
import random
import re
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils.html import strip_tags
from pprint import pprint
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token



class ObtainAuthTokenWithUser(ObtainAuthToken):
    """
    Handle authentication with JWT.
    """

    def post(self, request, *args, **kwargs):
        response = super(ObtainAuthTokenWithUser, self).post(request, *args, **kwargs)
        req_token = response.data['token']
        server_response = self.retrieve_info(req_token)
        return server_response

    def retrieve_info(self, token):
        token = Token.objects.get(key=token)
        user = User.objects.get(id=token.user_id)
        # Make sure user is active
        if user.is_active:
            return Response({'token': token.key, 'id': token.user_id, 'username':user.username})
        return Response({"error":"User is not active"}, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((permissions.IsAuthenticatedOrReadOnly,))
class Comment(generics.GenericAPIView, mixins.ListModelMixin, mixins.CreateModelMixin):
    """
    List all issue, or create a new issue.
    """
    
    serializer_class = CommentSerializer

    def perform_create(self, serializer):
        issue_id = self.request.data.get('issue', None)
        if not issue_id is None:
            try:
                issue = Issue.objects.get(id=issue_id).using('issues_db')
                serializer.save(creator=self.request.user, issue=issue, using='issues_db')
            except Issue.DoesNotExist:
                Response({"error":"Issue DoesNotExist"}, status=status.HTTP_400_BAD_REQUEST)
        Response({"error":"Issue number not known."}, status=status.HTTP_400_BAD_REQUEST)
        

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        # list is exposed by mixins.ListModelMixin
        return self.list(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        # create is exposed by mixins.CreateModelMixin
        return self.create(request, *args, **kwargs)

#class IssuesDBMixin(MultipleObjectMixin):
 #   def get_queryset(self):
  #      queryset = super(IssuesDBMixin, self).get_queryset()
   #     return queryset.using('issues_db')

@permission_classes((permissions.IsAuthenticatedOrReadOnly,))
class IssueList(generics.GenericAPIView, mixins.ListModelMixin, mixins.CreateModelMixin):
    """
    List all issue, or create a new issue.
    """
    
    serializer_class = IssueSerializer

    def get_queryset(self):
        """ 
        Overrides the default get_queryset method to allow for custom filters to be applied.
        """
        queryset = Issue.objects.all().using('issues_db')
        # Is there a location filter?
        location = self.kwargs.get('location', None)
        # Is there a user filter?
        pk = self.kwargs.get('pk', None)
        if not location is None:
            # Filter list by location
            queryset = queryset.using('issues_db').filter(location__icontains=strip_tags(location))
        if not pk is None:
            # Filter list by user id
            queryset = queryset.using('issues_db').filter(creator_id=strip_tags(pk))
        return queryset.using('issues_db')

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)#, using='issues_db')

    @csrf_exempt
    def get(self, request, *args, **kwargs):
        # list is exposed by mixins.ListModelMixin
        return self.list(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        # create is exposed by mixins.CreateModelMixin
        #print request.data
        return self.create(request, *args, **kwargs)


@permission_classes((permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,))
class IssueDetail(mixins.UpdateModelMixin, mixins.RetrieveModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):
    """
    Retrieve, update or delete a issue instance.
    """

    queryset = Issue.objects.all()
    serializer_class = IssueSerializer

    def get_queryset(self):
        return queryset.using('issues_db')


    def perform_create(self, serializer):
        serializer.save(creator=self.request.user, using='issues_db')


    def get(self, request, *args, **kwargs):
        
        return self.retrieve(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        # update is exposed by mixins.UpdateModelMixin        
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


@permission_classes((permissions.AllowAny,))
class UserActions(viewsets.ViewSet):
    """
    Actions that the user can perform, like deleting an issue or updating it. Signup create user
    """

    FIELDS = [req.name for req in get_user_model()._meta.fields]
    ACTIVE = "ACTIVE"




    def signup(self, request):

        if len(request.POST) != 0:
            submitted_data = request.POST
        elif len(request.data) != 0:
            submitted_data = request.data
        else:
            submitted_data = {}
        serialized = UserSerializer(data=submitted_data)
        if serialized.is_valid():
            data = {}
            for k, v in submitted_data.iteritems():
                # Check that input fields are valid.
                if k in self.FIELDS:
                    data[k] = v
            
            self._signup_inactive(**data)
            return Response({
                    "username": data['username']
                },
                status=status.HTTP_201_CREATED
            )
        else:
            #print serialized._errors
            return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)

    def _signup_inactive(self, username=None, email=None, password=None):
        """
        Starts the signup process which will end in sending an email with a token to confirm.
        """
        if username is not None:
            new_user = User.objects.create_user(username, email, password)
        else:
            new_user = User.objects.create_user(username=email, email=email, password=password)
        new_user.is_active = False
        new_user.save()

        citizen = self._create_temp_user(new_user)

        site = Site.objects.get_current()
        self._signup_send_activation_email(new_user, site)
        return new_user

    def _signup_send_activation_email(self, user, site):
        """
        Binds the context of the user request to an email to send the user to activate the account.
        """
        # Either read from settings or use in variable
        try:
            activation_path = settings.ACTIVATION_PATH
        except KeyError:
            activation_path = "activate"
        context = {
            'activation_key' : user.temporary_user_registration.activation_key,
            'expiration_days': 7,
            'activation_path': activation_path,
            'user'           : user,
            'site'           : site
        }
        subject = render_to_string(
            'registration/activation_email_subject.txt',
            context
        )

        subject = ''.join(subject.splitlines())
        message = render_to_string(
            'registration/activation_email.txt',
            context
        )
        
        user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

    def _create_temp_user(self, user):
        """
        Creates temporary user, before the porfile is activated.
        """
        activation_key = self.__generate_activation_key(user)
        citizen = TemporaryUser.objects.create(
            user=user, activation_key=activation_key)
        return citizen


    def __generate_activation_key(self, user):
        """
        Generates an activation key.
        """
        activation_key = hashlib.md5(str(random.random()).encode('utf-8')+user.USERNAME_FIELD).hexdigest()
        return activation_key


    def signup_complete(self, request):
        """
        Redirects user after successful verification of activation key.
        """
        try:
            activation_key = request.data['activation_key']
            registered = self._register_citizen(activation_key)
        except KeyError:
            registered = False
        if registered:
            return Response({"email": registered.email}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "Activation code not found."}, status=status.HTTP_404_NOT_FOUND)


    def _register_citizen(self, activation_key):
        """
        Finalizes registration when user confirms email.
        """
        try:
            citizen = TemporaryUser.objects.get(activation_key=activation_key)
        except TemporaryUser.DoesNotExist:
            return False
        if not self.ACTIVE in citizen.activation_key:
            user = citizen.user
            user.is_active = True
            user.save()
            citizen.activation_key = "{}_{}".format(self.ACTIVE, citizen.activation_key)
            citizen.save()
            return user
        return False

@permission_classes((permissions.IsAuthenticated,))
class EditPassword(mixins.UpdateModelMixin, generics.GenericAPIView, mixins.RetrieveModelMixin):
    """
    Edit user password view.
    """
    serializer_class = EditPasswordSerializer
    model = User

    def patch(self, request, *args, **kwargs):
        self.user = self.request.user
        serializer = EditPasswordSerializer(data=request.data)
        if serializer.is_valid():
            current_password = serializer.data['current_password']
            new_password = serializer.data['new_password']
            if not self.user.check_password(current_password):
                return Response({"detail": "Wrong password and/or email."}, status=status.HTTP_400_BAD_REQUEST)
            self.user.set_password(new_password)
            self.user.save()
            return Response({"detail": "Password changed"}, status=status.HTTP_200_OK)
        else:
            return Response({"detail": "Request missing parameters"}, status=status.HTTP_400_BAD_REQUEST)

@permission_classes((permissions.IsAuthenticated,))
class UserList(generics.ListAPIView):
    """
    Gives the users lists
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer




@permission_classes((IsOwnerOrReadOnly,))
class UserDetail(generics.RetrieveAPIView):
    """
    Gives user information to populate profile.
    """
   
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        """
        Get user information
        """
        username = self.kwargs.get('username', None)
        if "me" in username:
            try:
                req_token = request.META['HTTP_AUTHORIZATION'].split()[1]
            except:
                # Fail silentily
                return Response ({}, status=status.HTTP_400_BAD_REQUEST)
            #print req_token
            token_api = ObtainAuthTokenWithUser()
            server_response = token_api.retrieve_info(req_token)
            return server_response

        elif not username is None: 
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                return Response({"error":"User not found."}, status=status.HTTP_400_BAD_REQUEST)
            if user.is_active:
                return Response({'username':user.username, 'id': user.id})
            else:
                return Response({"error":"User is not active"}, status=status.HTTP_400_BAD_REQUEST)


